#!/bin/bash

set -e

cat /root/hosts >> /etc/hosts
zypper refs && zypper ref
zypper --non-interactive in python3 python-xml nginx