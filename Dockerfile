FROM registry.suse.com/suse/sle15:15.3.17.5.33
arg repo
arg cert
arg hostname
arg hostfqdn
arg hostip

# Build the host table entry for suse manager server
RUN echo "$hostip $hostfqdn $hostname" > /root/hosts

# Import the crt file of RMT server
RUN echo "$cert" > /etc/pki/trust/anchors/RHN-ORG-TRUSTED-SSL-CERT.pem
RUN update-ca-certificates

# Copy repo
RUN echo "$repo" > /etc/zypp/repos.d/susemanager\:dockerbuild.repo

ADD add_packages.sh /root/add_packages.sh
#SHELL ["bin/bash","-c"]

# Remove container-suseconnect to avoid warning messages when running zypper
RUN rpm -e container-suseconnect

# Execute installation of required packages in one layer 
RUN chmod u+x /root/add_packages.sh && /bin/bash /root/add_packages.sh

CMD ["/bin/bash","-c","cat /etc/os-release"]
